class Availability

  attr_accessor :date
  attr_accessor :slots

  def initialize(date)
    @date = date.to_date
    @slots = []
  end

  def set_slots(new_slots)
    puts "new Slots " + new_slots.to_s
    @slots = new_slots.map{|slot| Time.parse(slot).strftime("%-H:%M")}
  end

  def to_json
    {'date' => @date.strftime("%Y/%m/%d"), 'slots' => (p @slots)}.to_json
  end

end