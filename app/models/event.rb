require 'ostruct'
require 'time_difference'
require 'json'

class Event < ActiveRecord::Base

  @openings = Array.new
  @appointments = Array.new

  def self.create(kind, starts_at, ends_at, weekly_recurring=false)

    if kind .eql? "opening"

      @openings.push(OpenStruct.new(:kind => kind, :starts_at => starts_at,
                                    :ends_at => ends_at,
                                    :weekly_recurring => weekly_recurring))
    else

      @appointments.push(OpenStruct.new(:kind => kind, :starts_at => starts_at, :ends_at => ends_at))

    end

  end

  def self.availabilities(starting_at)

    $availabilities = []

    next_openings = nextOpeningsFrom(@openings, starting_at)

    next_openings.each {
     |o|
      $a = Availability.new o.starts_at
      $opening_slots = slots o.starts_at, o.ends_at
      $a.set_slots get_available_slots($a.date, $opening_slots)
      $availabilities << $a
    }

    (starting_at.to_i .. (starting_at + 6.days).to_i).step(1.day) do |date|

      $exist = $availabilities.any?{
                |s|
                s.date.strftime("%Y/%m/%d") == Time.at(date).strftime("%Y/%m/%d")
               }

      if !$exist
        $a = Availability.new Time.at(date)
        $availabilities << $a
      end

    end

    $availabilities = $availabilities.sort_by { |s| s.date }

    return $availabilities

  end

  def self.slots (starts_at, ends_at)
    slots = Array.new

    (starts_at.to_i .. ends_at.to_i-30.minutes).step(30.minutes) do |date|
      $slot = Time.at(date).utc.strftime(("%FT%T"))
      slots << $slot
    end

    return slots
  end

  def self.get_available_slots(date, slots)

    $available_slots = slots.clone
    @appointments.each {
      |appointment|
      $unavailable_slots = slots appointment.starts_at, appointment.ends_at
      $available_slots = $available_slots - $unavailable_slots
    }

    return $available_slots;

  end

  def self.nextOpeningsFrom(openings, starting_date)

    result = []

    @openings.each {

      |opening|
      if opening.weekly_recurring

        opening.starts_at < starting_date ? result.push(next_occurence(opening, starting_date)) : result.push(opening)

      else if (opening.starts_at - starting_date).to_i < 7
             result.push(opening);
           end
      end
    }

    return result
  end

  def self.next_occurence(opening, from)

    $nextOccurrence = opening.clone
    $nextOccurrence.starts_at = from.next_week.advance(:days => opening.starts_at.cwday-1,
                                                       :hours => opening.starts_at.strftime("%H").to_i,
                                                       :minutes => opening.starts_at.strftime("%M").to_i)

    $nextOccurrence.ends_at = from.next_week.advance(:days => opening.ends_at.cwday-1,
                                                     :hours => opening.ends_at.strftime("%H").to_i,
                                                     :minutes => opening.ends_at.strftime("%M").to_i)
    return $nextOccurrence

  end

end