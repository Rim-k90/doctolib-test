# test/models/event_test.rb

require 'test_helper'

class EventTest < ActiveSupport::TestCase

  test "one simple test example" do
    Event.create 'opening', DateTime.parse("2014-08-04 09:30"), DateTime.parse("2014-08-04 12:30"), true
    Event.create 'appointment', DateTime.parse("2014-08-11 10:30"), DateTime.parse("2014-08-11 11:30")

    availabilities = Event.availabilities DateTime.parse("2014-08-10")
    assert_equal Date.new(2014, 8, 10), availabilities[0].date
    assert_equal [], availabilities[0].slots
    assert_equal Date.new(2014, 8, 11), availabilities[1].date
    assert_equal ["9:30", "10:00", "11:30", "12:00"], availabilities[1].slots
    assert_equal Date.new(2014, 8, 16), availabilities[6].date
    assert_equal 7, availabilities.length
  end

end